# coding:utf-8

import requests
import lxml.html
import time

#抓取梯子网试卷
page = requests.get('http://www.tizi.com/paper/paper_exam/get_question?exam_id=69353&sid=18').json()
doc = lxml.html.document_fromstring(page['html'])

log = open('read.log', 'a')
_time = time.localtime(time.time())
for idx, el in enumerate(doc.cssselect('img.pre_img')):
    print 'get %s' % el.attrib['src']
    start = time.time()
    with open('content.txt', 'w') as f:
        f.write(requests.get(el.attrib['src']).content)
    print >> log, ('%s -> read file : %s' % (time.strftime('[%m-%d]%H:%M:%S', _time)))
    print >> log, 'Spend : %.5fs' % (time.time() - start)