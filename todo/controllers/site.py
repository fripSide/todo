# coding:utf-8
from flask import Blueprint, render_template

bp = Blueprint('site', __name__)

@bp.route('/', methods=['GET', 'POST'])
def index():
    return render_template('site/index.html')

@bp.route('/<int:tid>/edit', methods=['GET', 'POST'])
def edit(tid):
    pass