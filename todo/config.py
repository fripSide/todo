# coding:utf-8

# database settings
SQLALCHEMY_DATABASE_URI = 'sqlite:///todo.db'

# app
DEBUG = True
SITE_STATIC_PREFIX = '/static/'


