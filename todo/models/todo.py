# coding:utf-8
from ._base import db
from datetime import datetime


class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), nullable=False)
    created = db.Column(db.Date, default=datetime.utcnow)
    finished = db.Column(db.Boolean, default=False)