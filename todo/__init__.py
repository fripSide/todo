# coding:utf-8

from flask import Flask
import config
import logging
import sys
import os
import hashlib

reload(sys)
sys.setdefaultencoding('utf8')


def create_app():
    app = Flask(__name__,
                template_folder='templates'
    )
    app.config.from_object(config)
    register_db(app)
    register_routes(app)
    register_jinja(app)
    register_logger(app)
    return app


def register_db(app):
    from .models import db

    db.init_app(app)
    db.app = app


def register_jinja(app):
    if not hasattr(app, '_static_hash'):
        app._static_hash = {}

    def static_url(filename):
        if app.testing:
            return filename

        if filename in app._static_hash:
            return app._static_hash[filename]

        with open(os.path.join(app.static_folder, filename), 'r') as f:
            content = f.read()
            hsh = hashlib.md5(content).hexdigest()

        app.logger.info('Generate %s md5sum: %s' % (filename, hsh))
        prefix = app.config.get('SITE_STATIC_PREFIX', '/static/')
        value = '%s%s?v=%s' % (prefix, filename, hsh[:8])
        app._static_hash[filename] = value
        return value

    def link(filename):
        uri = static_url(filename)
        return '<link rel="stylesheet" href="%s"></link>' % uri

    @app.context_processor
    def register_context():
        return dict(
            link=link
        )

    return app


def register_logger(app):
    if not app.debug:
        return
    handler = logging.StreamHandler()
    handler.setLevel(logging.ERROR)
    app.logger.addHandler(handler)
    return app


def register_routes(app):
    from .controllers import site

    app.register_blueprint(site.bp, url_prefix='/')
    return app