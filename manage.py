# coding:utf-8
from todo import create_app
from flask_script import Manager, Server

manager = Manager(create_app)
manager.add_command('run', Server())
manager.add_command('run80', Server(port=80))


@manager.command
def create_db():
    from todo.models import db

    db.create_all()


if __name__ == '__main__':
    manager.run()